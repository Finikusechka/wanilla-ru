# Командный проект ГБ
![alt_text](https://k9scli.io/assets/k9s.png)

# Коммерческое предложение
Данное коммерческое предложение учебное. Принятие Вами каких-либо пунктов и/или положений настоящего коммерческого предложения, не побуждает каких-либо юридических последствий.

## Инфраструктура


Схема взаимодействия узлов и интернета

![alt_text](imgs/gb_team_project.png)

## Требования для развёртывания


1. Площадка размещения инфраструктуры - облачные ресурсы VK Cloud Solution

2. Количество виртуальных машин (ВМ) в облаке - 12 шт.

3. Количество балансировщиков нагрузки - 2 шт.

4. Количество плавающих IP-адресов - 2 шт.

5. Количство объектных хранилищ - 1 шт.

## Логическое разделение вычислительных ресурсов


- Управляющая ВМ (мастер-хост кубернетес) - 1 шт. OS - Ubuntu 20.04 LTS. CPU - 2 шт. RAM - 4 ГБайт. SSD - 20 ГБайт.

- Рабочие ВМ (кубернетес) - OS - Ubuntu 20.04 LTS. 3 шт. CPU - 2 шт. RAM - 4 ГБайт. SSD - 20 ГБайт.

- ВМ для кластера Galera MariaDB - OS - Ubuntu 20.04 LTS. 4 шт. SSD - 20 ГБайт.

- ВМ административный хост - 1 шт. CPU = 1 шт. RAM 1 ГБайт. SSD - 20 ГБайт.

- ВМ для файлового хранилища - 1 шт. CPU = 1 шт. RAM 1 ГБайт. SSD - 20 ГБайт.

- Балансировщик нагрузки рабочих ВМ - 1 шт.

- Балансировщик нагрузки для кластера БД Galera MariaDB - 1 шт.

- Плавающий IP-адрес для административного хоста - 1 шт.

- Плавающий IP-адрес для балансировщика нагрузки рабочих ВМ - 1 шт.

- Объектное хранилище для нужд gitlab-runner - 1 шт.

## Описание взаимодействия с узлами из интернета


Обслуживающий персонал подключается к административному хосту и выполняет операции по развёртыванию и последующему сопровождению инфраструктуры. Подключение производится по протоколу SSH.

Приложение разворачивается на двух узлах кластера кубернетес. У ВМ кластера кубернетес отсутствует возможность принимать какой-либо входящий трафик из интернета. Входящий трафик поступает на балансировщик нагрузки. Балансировщик нагрузки обеспечивает балансировку трафика на узлы кластера кубернетес. Балансировщик нагрузки принимает входящий трафик на порты 443 и 80.

Кластер БД Galera MariaDB разворачивается на 4-х ВМ площадки размещения инфраструктуры. Данный кластер использует режим multi-master, который обеспечивает синхронную репликацию. Этот режим репликации обеспечивает высокую доступность данных, параллельное выполнение транзакций и гарантию причинно-следственной связи во всём кластере. Что в свою очередь обеспечивает отказоустойчивость.

Кластеры кубернетес и БД размещаются за балансировщиками нагрузки.

Административный хост - это хост, с которого осуществляется настройка всей инфраструктуры. Административный хост принимает входящий трафик на порт 22. После первоначального развёртывания инфраструктуры, можно доступ на порт 22 закрыть. И в дальнейшем использовать консоль управления хостом площадки размещения инфраструктуры.

Для первоначального разворачивания административного хоста, необходим подключенный gitlab-runner к репозиторию исходного кода разворачиваемой инфраструктуры.

После первоначального развёртывания административного хоста, на административный хост устанавливается gitlab-runner, с помощью которого производится дальнейшая конфигурация кластера кубернетес.

Отказоустойчивость кластера Кубернетес обеспечивается за счет избыточности ВМ для рабочей нагрузки.

Отказоустойчивость приложения обеспечивается за счет развёртывания нескольких экземпляров приложения в кластере кубернетес на разных ВМ.

Целостность данных файлового хранилища обеспечивается штатными средствами площадки размещения инфраструктуры.

Целостность данных объектного хранилища обеспечивается штатными средствами площадки размещения инфраструктуы.

Целостность данных административного хоста обеспечивается штатными средствами площадки размещения инфраструктуры.

## Жизненный цикл Пользовательского запроса
Пользовательский запрос из интернета, приходит на балансировщик кубернетеса, который перенаправляет его на один из узлов кластера  кубернетес.

Узел кластера кубернетес, в процессе обработки пользовательского запроса обращается к кластеру БД Galera Maria DB через балансировщик кластера БД.

После обработки пользовательского запроса, узел кластера кубернетес возвращает ответ на балансировщик кубернетес. Балансировщик кубернетес, в свою очередь возвращает ответ полученный от узла кубернетес рабочей нагрузки пользователю, иницировавшему пользовательский запрос.

## Кластер базы данных
Кластер БД Galera Maria DB развернут на отдельных хостах. Кластер сконфигурирован в режиме мульти-мастер.

В кластере кубернетес запущено задание бекапа БД по расписанию. В назначенный час оно отправляет в кластер БД запрос на создание бэкапа базы данных. Кластер базы данных создаёт бекап базы данных, архивирует его и помещает его на хранение в файловое хранилище.

Также в кластере кубернетес предусмотрены задания для ручного создания и восстановления бекапа базы данных. Процедура создания бекапа базы данных в ручном режиме, аналогична процедуре создания бекапа базы данных заданием по расписанию.

При восстановлении базы данных из бекапа, в кластере БД запускается задание для восстановления бекапа базы данных из архива. Для выполнения задания восстановления базы данных из архива необходимо при запуске задания указать имя архива из которого необходимо произвести восстановления базы данных.

В случае успеха/неудачи создания бэкапа БД, кластер кубернетес отправляет сообщение в телеграмм бот с результатом выполненного задания.

## Пайплайн
Пайплайн логически разделен на этапы.

1. Этап развертывания инфраструктуры. На данном этапе производится создание облачных ресурсов на площадке.

2. Этап подготовки административного хоста. На данном этапе выполняются все необходимые задания по настройке административного хоста, с которого будет производится развёртывание облачной инфраструктуры.

3. Этап подготовки инфраструктуры. На данном этапе производится обновление пакетов на хостах инфраструктуры, развёртывание кластера БД Galera MariaDB, настройка файловго хранилища.

4. Этап подготовки кластера кубернетес. На данном этапе производится установка пакетов кубернетеса, установка среды исполнения контейнеров, развёртывание и первоначальная конфигурация кластера кубернетес.

5. Этап развёртывания приложений в Кубернетес. На данном этапе производится установка приложений в кубернетес. Устанавливается само приложение Wordpress, приложение телеграмм бот и экспортер мертик.

6. Этап переконфигурации кластера кубернетес. Данный этап предназначен для повседневной работы с кластером кубернетес.

7. Этап создания бекапа БД в ручную.

8. Этап восстановления БД из бекапа в ручную.

9. Этап уничтожения ресурсов на площадке размещения инфраструктуры.

## Переменные пайплайна:
- CI_DEBUG_TRACE - Переменная дла активации отладочной информации в gitlab-runner.

- CI_CREATE_METAL - Переменная для активации этапа развёртывания инфраструктуры.

- CI_PREPARE_ADMIN_HOST - Переменная для активации этапа подготовки административного хоста.

- CI_UPDATE_INFRA_HOSTS - Переменная для активации этапа подготовки инфраструктуры.

- CI_PREPARE_KUBE_CLUSTER - Переменная для активации этапа подготовки кластера кубернетес.

- CI_DESTROY_METAL - Переменная для активации этапа уничтожения ресурсов на площадке размещения инфраструктуры.

- CI_BACKUP_DB - Переменная дла активации задания по созданию бекапа БД в ручном режиме.

- CI_RESTORE_DB - Переменная дла активации задания по восстановлению бекапа БД в ручном режиме.

- CI_DEVELOP_MODE - Техническая переменная. В продуктивном режиме ни на что не влияет. Предназначена для процесса разработки и отладки пайплайна.

## Система управления конфигурациями
- Управление вычислительными ресурсами - Terraform

- Настройка вычислительных ресурсов - Ansible

- Управление системами управления конфигурациями - Gitlab-runner

## Система мониторина
- Grafana, Prometheus, Loki.

- Агенты мониторинга node_exorter.

