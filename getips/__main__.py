import os
import json

os.environ.update(OS_AUTH_URL="https://infra.mail.ru:35357/v3/", OS_PROJECT_ID="163206f811634c489ce7471f272f205a",
                  OS_REGION_NAME="RegionOne", OS_USER_DOMAIN_NAME="users",
                  OS_USERNAME=os.environ.get('TF_VAR_CI_CLOUD_LOGIN'), OS_PASSWORD=os.environ.get('TF_VAR_CI_CLOUD_PASSWORD'),
                  )


# def get_host_var_name(key: str):
#     host_map_dict = {
#         'gb-admin': 'ip_gb_admin',
#         'gb-kube-db': 'ip_gb_kube_db',
#         'gb-kube-m1': 'ip_gb_kube_m1',
#         'gb-kube-n1': 'ip_gb_kube_n1',
#         'gb-kube-n2': 'ip_gb_kube_n2',
#         'gb-kube-vols': 'ip_gb_kube_vols'
#     }

#     return host_map_dict.get(key)

def ip_preparator(data: str):
    s = data.replace('network-net=', '').replace(',', '').split()

    return s[1]
    # for i in s:
    #     print(i)
        # if i.startswith('192.168'):
        #     return i

def get_admin_host_net(data: list):
    for i in data:
        if i.get('Name') != 'gb-admin':
            continue
        return i.get('Networks')


def generate_inventory(exi_ip: str):
    pass

def main():
    print('Hello IPs')
    result = os.popen('openstack server list -f json').read()

    j = json.loads(result)

    ext_ip = ip_preparator(get_admin_host_net(j))
    print('External ip is:', ext_ip)
    gen_inv = f'''
all:
  vars:
    ansible_become: yes
    ansible_become_method: sudo
    ansible_become_user: root
    ansible_connection: ssh
    ansible_ssh_user: ubuntu
  children:
    admin_grp:
      children:
        admin:
          vars:
            ansible_ssh_private_key_file: ~/Ubuntu20_04.pem
          hosts:
            {ext_ip}:
    '''

    # ips = dict()
    # counter = 0
    # for i in j:
    #     ips.update(
    #         {counter: {'name': i.get('Name'), 'ip': ip_preparator(i.get('Networks'))}})
    #     counter += 1

    

    # with open('group_vars/all/vars.yml', 'w') as f:
    #     for i in ips.values():
    #         f.write(f"{get_host_var_name(i.get('name'))}: {i.get('ip')}\n")

    #     f.close()

    with open('g_inventory.yml', 'w', encoding='utf-8') as f:
        f.write(gen_inv)
        f.close()

if __name__ == '__main__':
    main()
