import unittest

class TestRules():
    @staticmethod
    def get_environment(ci_create_metal: bool, ci_destroy_metal: bool, ci_prepare_admin_host: bool, ci_develop_mode: bool, ci_prepare_kube_cluster: bool, ci_update_infra_hosts: bool, ci_backup_db: bool, ci_restore_db: bool):
        return {'ci_create_metal': ci_create_metal, 'ci_destroy_metal': ci_destroy_metal, 'ci_prepare_admin_host': ci_prepare_admin_host, 'ci_develop_mode': ci_develop_mode, 'ci_prepare_kube_cluster': ci_prepare_kube_cluster, 'ci_update_infra_hosts': ci_update_infra_hosts, 'ci_backup_db': ci_backup_db, 'ci_restore_db': ci_restore_db}

    @staticmethod
    def create_metal_rule(ci_create_metal: bool, ci_destroy_metal: bool, ci_backup_db: bool, ci_restore_db: bool):
        if ci_create_metal and not ci_destroy_metal and not ci_backup_db and not ci_restore_db:
            return True

        return False
    
    @staticmethod
    def destroy_metal_rule(ci_create_metal: bool, ci_destroy_metal: bool, ci_backup_db: bool, ci_restore_db: bool):
        if not ci_create_metal and ci_destroy_metal and not ci_backup_db and not ci_restore_db:
            return True

        return False

    @staticmethod
    def prepare_admin_host_rule(ci_prepare_admin_host: bool, ci_destroy_metal: bool, ci_backup_db: bool, ci_restore_db: bool):
        if ci_prepare_admin_host and not ci_destroy_metal and not ci_backup_db and not ci_restore_db:
            return True
        
        return False

    @staticmethod
    def rule_prepare_kube_cluster(ci_destroy_metal: bool, ci_prepare_kube_cluster: bool, ci_develop_mode: bool, ci_backup_db: bool, ci_restore_db: bool):
        if not ci_destroy_metal and ci_prepare_kube_cluster and not ci_develop_mode and not ci_backup_db and not ci_restore_db:
            return True
        
        return False

    @staticmethod
    def rule_update_infra_hosts(ci_update_infra_hosts: bool, ci_destroy_metal: bool, ci_develop_mode: bool, ci_backup_db: bool, ci_restore_db: bool):
        if ci_update_infra_hosts and not ci_destroy_metal and not ci_develop_mode and not ci_backup_db and not ci_restore_db:
            return True
        
        return False

    @staticmethod
    def rule_for_deploy_app(ci_create_metal: bool, ci_destroy_metal: bool, ci_prepare_kube_cluster: bool, ci_update_infra_hosts: bool, ci_prepare_admin_host: bool, ci_develop_mode: bool, ci_backup_db: bool, ci_restore_db: bool):
        if not ci_create_metal and not ci_destroy_metal and not ci_prepare_kube_cluster and not ci_update_infra_hosts and not ci_prepare_admin_host and not ci_develop_mode and not ci_backup_db and not ci_restore_db:
            return True
        
        return False
    
    @staticmethod
    def rule_for_develop(ci_create_metal: bool, ci_destroy_metal: bool, ci_develop_mode: bool, ci_backup_db: bool, ci_restore_db: bool):
        if not ci_create_metal and not ci_destroy_metal and ci_develop_mode and not ci_backup_db and not ci_restore_db:
            return True
        
        return False

    @staticmethod
    def rule_for_manual_backup_db(ci_backup_db: bool, ci_restore_db: bool):
        if ci_backup_db and not ci_restore_db:
            return True
        
        return False

    @staticmethod
    def rule_for_manual_restore_db(ci_backup_db: bool, ci_restore_db: bool):
        if not ci_backup_db and ci_restore_db:
            return True
        
        return False

    @staticmethod
    def rule_for_develop(ci_create_metal: bool, ci_destroy_metal: bool, ci_develop_mode: bool, ci_backup_db: bool, ci_restore_db: bool):
        if not ci_create_metal and not ci_destroy_metal and ci_develop_mode and not ci_backup_db and not ci_restore_db:
            return True
        
        return False

class TestRules_test(unittest.TestCase):
    def initialize(self, env: dict):
        ci_create_metal = env.get('ci_create_metal')
        ci_destroy_metal = env.get('ci_destroy_metal')
        ci_prepare_admin_host = env.get('ci_prepare_admin_host')
        ci_develop_mode = env.get('ci_develop_mode')
        ci_prepare_kube_cluster = env.get('ci_prepare_kube_cluster')
        ci_update_infra_hosts = env.get('ci_update_infra_hosts')
        ci_backup_db = env.get('ci_backup_db')
        ci_restore_db = env.get('ci_restore_db')
        
        self.J_CREATE_METAL = TestRules.create_metal_rule(ci_create_metal, ci_destroy_metal, ci_backup_db, ci_restore_db)
        self.J_DESTROY_METAL = TestRules.destroy_metal_rule(ci_create_metal, ci_destroy_metal, ci_backup_db, ci_restore_db)
        self.J_GET_ADMIN_HOST_EXT_IP = TestRules.prepare_admin_host_rule(ci_prepare_admin_host, ci_destroy_metal, ci_backup_db, ci_restore_db)
        self.J_PREPARE_ADMIN_HOST = TestRules.prepare_admin_host_rule(ci_prepare_admin_host, ci_destroy_metal, ci_backup_db, ci_restore_db)
        self.J_SSH_ACCESS = TestRules.prepare_admin_host_rule(ci_prepare_admin_host, ci_destroy_metal, ci_backup_db, ci_restore_db)
        self.J_PREPARE_INFRA_HOSTS = TestRules.rule_update_infra_hosts(ci_update_infra_hosts, ci_destroy_metal, ci_develop_mode, ci_backup_db, ci_restore_db)
        self.J_PREPARE_NFS_HOST = TestRules.rule_update_infra_hosts(ci_update_infra_hosts, ci_destroy_metal, ci_develop_mode, ci_backup_db, ci_restore_db)
        self.J_PREPARE_KUBERNETES_HOSTS = TestRules.rule_prepare_kube_cluster(ci_destroy_metal, ci_prepare_kube_cluster, ci_develop_mode, ci_backup_db, ci_restore_db)
        self.J_BOOTSTRAP_KUBE_MASTER_NODE = TestRules.rule_prepare_kube_cluster(ci_destroy_metal, ci_prepare_kube_cluster, ci_develop_mode, ci_backup_db, ci_restore_db)
        self.J_JOIN_NODES = TestRules.rule_prepare_kube_cluster(ci_destroy_metal, ci_prepare_kube_cluster, ci_develop_mode, ci_backup_db, ci_restore_db)
        self.J_PING_INFRA_HOSTS = TestRules.prepare_admin_host_rule(ci_prepare_admin_host, ci_destroy_metal, ci_backup_db, ci_restore_db)
        self.J_ACCESS_FOR_KUBE = TestRules.rule_prepare_kube_cluster(ci_destroy_metal, ci_prepare_kube_cluster, ci_develop_mode, ci_backup_db, ci_restore_db)
        self.J_DEPLOY_INGRESS_CONTROLLER = TestRules.rule_prepare_kube_cluster(ci_destroy_metal, ci_prepare_kube_cluster, ci_develop_mode, ci_backup_db, ci_restore_db)
        self.J_DEPLOY_APP_NGINX = TestRules.rule_for_deploy_app(ci_create_metal, ci_destroy_metal, ci_prepare_kube_cluster, ci_update_infra_hosts, ci_prepare_admin_host, ci_develop_mode, ci_backup_db, ci_restore_db)
        self.J_DEPLOY_APP_WORDPRESS = TestRules.rule_for_deploy_app(ci_create_metal, ci_destroy_metal, ci_prepare_kube_cluster, ci_update_infra_hosts, ci_prepare_admin_host, ci_develop_mode, ci_backup_db, ci_restore_db)
        self.J_DEPLOY_TELEGRAM_BOT = TestRules.rule_for_deploy_app(ci_create_metal, ci_destroy_metal, ci_prepare_kube_cluster, ci_update_infra_hosts, ci_prepare_admin_host, ci_develop_mode, ci_backup_db, ci_restore_db)
        self.J_BACKUP_DB = TestRules.rule_for_manual_backup_db(ci_backup_db, ci_restore_db)
        self.J_RESTORE_DB = TestRules.rule_for_manual_restore_db(ci_backup_db, ci_restore_db)
        self.J_DEVELOP_JOB = TestRules.rule_for_develop(ci_create_metal, ci_destroy_metal, ci_develop_mode, ci_backup_db, ci_restore_db)

    def get_result(self):
        return [self.J_CREATE_METAL,                    self.J_DESTROY_METAL, 
                self.J_GET_ADMIN_HOST_EXT_IP,           self.J_PREPARE_ADMIN_HOST, 
                self.J_SSH_ACCESS,                      self.J_PREPARE_INFRA_HOSTS, 
                self.J_PREPARE_NFS_HOST,                self.J_PREPARE_KUBERNETES_HOSTS, 
                self.J_BOOTSTRAP_KUBE_MASTER_NODE,      self.J_JOIN_NODES, 
                self.J_PING_INFRA_HOSTS,                self.J_ACCESS_FOR_KUBE, 
                self.J_DEPLOY_INGRESS_CONTROLLER,       self.J_DEPLOY_APP_NGINX,
                self.J_DEPLOY_APP_WORDPRESS,            self.J_DEPLOY_TELEGRAM_BOT,
                self.J_BACKUP_DB,                       self.J_RESTORE_DB,
                self.J_DEVELOP_JOB]

    def test_only_create_metal_true(self):
        env = TestRules.get_environment(True, False, False, False, False, False, False, False)
        self.initialize(env)

        result = self.get_result()

        result_expected = [True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]

        self.assertListEqual(result, result_expected, 'Должны быть задания только по развертыванию хостов')

    def test_only_prepare_admin_true(self):
        env = TestRules.get_environment(False, False, True, False, False, False, False, False)
        self.initialize(env)

        result = self.get_result()
        
        result_expected = [False, False, True, True, True, False, False, False, False, False, True, False, False, False, False, False, False, False, False]

        self.assertListEqual(result, result_expected, 'Должны быть задания только по подготовке административного хоста и установки ключа доступа')
    
    def test_only_update_infra_hosts_true(self):
        env = TestRules.get_environment(False, False, False, False, False, True, False, False)
        self.initialize(env)

        result = self.get_result()

        result_expected = [False, False, False, False, False, True, True, False, False, False, False, False, False, False, False, False, False, False, False]

        self.assertListEqual(result, result_expected, 'Должно сработать только задание на обновление пакетов на хостах инфраструктуры и перезагрузку всей инфраструктуры')

    def test_only_prepare_kube_cluster_true(self):
        env = TestRules.get_environment(False, False, False, False, True, False, False, False)
        self.initialize(env)

        result = self.get_result()

        result_expected = [False, False, False, False, False, False, False, True, True, True, False, True, True, False, False, False, False, False, False]

        self.assertListEqual(result, result_expected, 'Должно сработать только задание подготовку кубернетес кластера')

    def test_create_metal_and_prepare_admin_true(self):
        env = TestRules.get_environment(True, False, True, False, False, False, False, False)
        self.initialize(env)

        result = self.get_result()

        result_expected = [True, False, True, True, True, False, False, False, False, False, True, False, False, False, False, False, False, False, False]

        self.assertListEqual(result, result_expected, 'Должны быть задания только по развертыванию хостов, подготовке административного хоста и установки ключа доступа')

    def test_create_metal_and_prepare_admin_and_update_infra_hosts_true(self):
        env = TestRules.get_environment(True, False, True, False, False, True, False, False)
        self.initialize(env)

        result = self.get_result()
        
        result_expected = [True, False, True, True, True, True, True, False, False, False, True, False, False, False, False, False, False, False, False]

        self.assertListEqual(result, result_expected, 'Должны быть задания только по развертыванию хостов, подготовке административного хоста и установки ключа доступа, обновлению инфраструктурных хостов и перезагрузка инфраструктурных хостов')
    
    def test_only_deploy_to_kube_cluster(self):
        env = TestRules.get_environment(False, False, False, False, False, False, False, False)
        self.initialize(env)

        result = self.get_result()

        result_expected = [False, False, False, False, False, False, False, False, False, False, False, False, False, True, True, True, False, False, False]

        self.assertListEqual(result, result_expected, 'Должны быть задания только на деплой приложений в кубернетес')

    def test_j_destroy_metal(self):
        env = TestRules.get_environment(False, True, False, False, False, False, False, False)
        self.initialize(env)

        result = self.get_result()

        result_expected = [False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]
        
        self.assertListEqual(result, result_expected, 'Должно быть только одно задание Destroy metal')

    def test_develop_mode(self):
        env = TestRules.get_environment(False, False, False, True, False, False, False, False)
        self.initialize(env)

        result = self.get_result()

        result_expected = [False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True]

        self.assertListEqual(result, result_expected, 'Должны быть только задания, которые помечены как разрабатываемые')

    def test_only_backup_db(self):
        env = TestRules.get_environment(False, False, False, False, False, False, True, False)
        self.initialize(env)

        result = self.get_result()

        result_expected = [False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, False, False]

        self.assertListEqual(result, result_expected, 'Должна быть джоба на бэкап БД')

    def test_only_restore_db(self):
        env = TestRules.get_environment(False, False, False, False, False, False, False, True)
        self.initialize(env)

        result = self.get_result()

        result_expected = [False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, False]

        self.assertListEqual(result, result_expected, 'Должна быть джоба на бэкап БД')


    # Тесты на всякий несуразный архаизм
    def test_create_and_destroy_true(self):
        env = TestRules.get_environment(True, True, False, False, False, False, False, False)
        self.initialize(env)

        result = self.get_result()

        result_expected = [False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]

        self.assertListEqual(result, result_expected, 'Все должно быть false')

    def test_create_and_develop_true(self):
        env = TestRules.get_environment(True, False, False, True, False, False, False, False)
        self.initialize(env)

        result = self.get_result()

        result_expected = [True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]

        self.assertListEqual(result, result_expected, 'При таких условиях должна сгенерироваться только одна джоба на создание инфраструктуры')

    def test_destroy_and_develop_true(self):
        env = TestRules.get_environment(False, True, False, True, False, False, False, False)
        self.initialize(env)

        result = self.get_result()

        result_expected = [False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]

        self.assertListEqual(result, result_expected, 'При таких условиях должна сгенерироваться только одна джоба на уничтожение инфраструктуры')

    def test_all_true(self):
        env = TestRules.get_environment(True, True, True, True, True, True, False, False)
        self.initialize(env)

        result = self.get_result()

        result_expected = [False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]

        self.assertListEqual(result, result_expected, 'Если все в True, то все должно быть False')

    def test_restore_and_backup_db(self):
        env = TestRules.get_environment(False, False, False, False, False, False, True, True)
        self.initialize(env)

        result = self.get_result()

        result_expected = [False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]

        self.assertListEqual(result, result_expected, 'Должно быть все False')

 
if __name__ == '__main__':
    unittest.main()

