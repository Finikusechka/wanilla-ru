from flask import Flask
from flask import Flask, request
from telegram_bot.classes.tg_bot import TelegramBot as tg
import json
import os

app = Flask(__name__)


@app.route('/message', methods=['GET', 'POST'])
def send_message():
    data = json.loads(request.data)

    if data.get('object_kind') == 'push':
        result = f"Событие в репозитории проекта: {data.get('project').get('name')}\n\nЧеловек по имени: {data.get('user_name')}\nСделал изменение типа: {data.get('object_kind')}\nИзменение сделано на ветке: {data.get('ref')}\nХэш комита: {data.get('checkout_sha')}\n{data.get('user_avatar')}"
    elif data.get('object_kind') == 'pipeline':
        result = f"Событие в репозитории проекта: {data.get('project').get('name')}\n\nЧеловек по имени: {data.get('user').get('name')}\nЗапустил пайплайн на ветке: {data.get('object_attributes').get('ref')}\nСтатус пайплайна: {data.get('object_attributes').get('status')}\nХэш комита: {data.get('commit').get('id')}\n{data.get('user').get('avatar_url')}"
    elif data.get('object_kind') == 'cronjob':
        result = f"Событие задания по расписанию в кубернтесе: {data.get('job_name')}\n\nКубернетес выполнил: {data.get('job_name')}\nДата выполнения: {data.get('date')}\nРезультат выполнения задания: {data.get('result')}"
    elif data.get('object_kind') == 'job':
        result = f"Событие ручное одноразовое задание в кубернтесе: {data.get('job_name')}\n\nКубернетес выполнил: {data.get('job_name')}\nДата выполнения: {data.get('date')}\nРезультат выполнения задания: {data.get('result')}"
    else:
        result = data

    tg_worker = tg(os.environ.get('TG_BOT_TOKEN'),
                   os.environ.get('TG_CHAT_ID'))

    test = tg_worker.telegram_bot_sendtext(result)
    return test


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8080')
