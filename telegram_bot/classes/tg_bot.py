import requests

class TelegramBot():
    
    def telegram_bot_sendtext(self, bot_message):
        bot_token = self.__bot_token
        bot_chat_id = self.__bot_chat_id
        send_text = f'https://api.telegram.org/bot{bot_token}/sendMessage?chat_id={bot_chat_id}&text={bot_message}'

        response = requests.get(send_text)

        return response.json()
    
    def __init__(self, bot_token, bot_chat_id):
        self.__bot_token = bot_token
        self.__bot_chat_id = bot_chat_id
