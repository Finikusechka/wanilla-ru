class md_wrapper():
    @staticmethod
    def md_header_1(data: str):
        return f'# {data}\n'
    
    @staticmethod
    def md_header_2(data: str):
        return f'## {data}\n'
    
    @staticmethod
    def md_header_3(data: str):
        return f'### {data}\n'
    
    @staticmethod
    def md_add_n():
        return '\n'
    
    @staticmethod
    def md_new_line():
        return '\n\n'
    
    @staticmethod
    def md_code_block(data: str):
        return f'```\n{data}\n```'
    
    @staticmethod
    def md_code_block_by_list(data: list):
        print(data)
        begin = '```\n'
        body = str()
        end = '```'
        
        body = begin
        for i in data:
            print(i)
            body += i + '\n'
        
        body += end

        print(body)
        return body

    @staticmethod
    def md_digit_enum(data: list):
        count = 1
        result = str()
        for i in data:
            result += f'{count}. {i}\n'
            count +=1
        
        return result
    
    @staticmethod
    def get_image_by_url(data: str):
        return f'![alt_text]({data})'
    