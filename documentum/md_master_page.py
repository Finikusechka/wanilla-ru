import documentum.docstrings as ds
import md_wrapper

def page_appender(*args):
    page = str()
    for i in args:
        page += i
    
    return page

def get_page():
    md_wrk = md_wrapper.md_wrapper()

    the_page = page_appender(
        md_wrk.md_header_1(ds.page_title),
        md_wrk.get_image_by_url(ds.kuber_image), md_wrk.md_new_line(),
        md_wrk.md_header_1(ds.expl_guide_title),
        ds.expl_guide_p1, md_wrk.md_new_line(),
        md_wrk.md_header_2(ds.infra_title), md_wrk.md_new_line(),
        ds.infra_scheme_p1, md_wrk.md_new_line(),
        md_wrk.get_image_by_url(ds.infra_scheme), md_wrk.md_new_line(),
        md_wrk.md_header_2(ds.infra_pre_title), md_wrk.md_new_line(),
        ds.infra_p1, md_wrk.md_new_line(),
        ds.infra_p2, md_wrk.md_new_line(),
        ds.infra_p3, md_wrk.md_new_line(),
        ds.infra_p4, md_wrk.md_new_line(),
        ds.infra_p5, md_wrk.md_new_line(),
        md_wrk.md_header_2(ds.infra_logic_title), md_wrk.md_new_line(),
        ds.infra_logic_p1, md_wrk.md_new_line(),
        ds.infra_logic_p2, md_wrk.md_new_line(),
        ds.infra_logic_p3, md_wrk.md_new_line(),
        ds.infra_logic_p4, md_wrk.md_new_line(),
        ds.infra_logic_p5, md_wrk.md_new_line(),
        ds.infra_logic_p6, md_wrk.md_new_line(),
        ds.infra_logic_p7, md_wrk.md_new_line(),
        ds.infra_logic_p8, md_wrk.md_new_line(),
        ds.infra_logic_p9, md_wrk.md_new_line(),
        ds.infra_logic_p10, md_wrk.md_new_line(),
        md_wrk.md_header_2(ds.logic_infra_title), md_wrk.md_new_line(),
        ds.logic_p1, md_wrk.md_new_line(),
        ds.logic_p2, md_wrk.md_new_line(),
        ds.logic_p3, md_wrk.md_new_line(),
        ds.logic_p4, md_wrk.md_new_line(),
        ds.logic_p5, md_wrk.md_new_line(),
        ds.logic_p6, md_wrk.md_new_line(),
        ds.logic_p7, md_wrk.md_new_line(),
        ds.logic_p8, md_wrk.md_new_line(),
        ds.logic_p9, md_wrk.md_new_line(),
        ds.logic_p10, md_wrk.md_new_line(),
        ds.logic_p11, md_wrk.md_new_line(),
        ds.logic_p12, md_wrk.md_new_line(),
        ds.logic_p13, md_wrk.md_new_line(),
        md_wrk.md_header_2(ds.user_requests_title),
        ds.user_requests_p1, md_wrk.md_new_line(),
        ds.user_requests_p2, md_wrk.md_new_line(),
        ds.user_requests_p3, md_wrk.md_new_line(),
        md_wrk.md_header_2(ds.db_title),
        ds.db_p1, md_wrk.md_new_line(),
        ds.db_p2, md_wrk.md_new_line(),
        ds.db_p3, md_wrk.md_new_line(),
        ds.db_p4, md_wrk.md_new_line(),
        ds.db_p5, md_wrk.md_new_line(),
        ds.db_p6, md_wrk.md_new_line(),
        ds.db_p7, md_wrk.md_new_line(),
        md_wrk.md_header_2(ds.pipeline_title),
        ds.pipeline_p1, md_wrk.md_new_line(),
        ds.pipeline_p2, md_wrk.md_new_line(),
        ds.pipeline_p3, md_wrk.md_new_line(),
        ds.pipeline_p4, md_wrk.md_new_line(),
        ds.pipeline_p5, md_wrk.md_new_line(),
        ds.pipeline_p6, md_wrk.md_new_line(),
        ds.pipeline_p7, md_wrk.md_new_line(),
        ds.pipeline_p8, md_wrk.md_new_line(),
        ds.pipeline_p9, md_wrk.md_new_line(),
        ds.pipeline_p10, md_wrk.md_new_line(),
        md_wrk.md_header_2(ds.pipeline_var_title),
        ds.pipeline_var_p1, md_wrk.md_new_line(),
        ds.pipeline_var_p2, md_wrk.md_new_line(),
        ds.pipeline_var_p3, md_wrk.md_new_line(),
        ds.pipeline_var_p4, md_wrk.md_new_line(),
        ds.pipeline_var_p5, md_wrk.md_new_line(),
        ds.pipeline_var_p6, md_wrk.md_new_line(),
        ds.pipeline_var_p7, md_wrk.md_new_line(),
        ds.pipeline_var_p8, md_wrk.md_new_line(),
        ds.pipeline_var_p9, md_wrk.md_new_line(),
        md_wrk.md_header_2(ds.manage_config_system_title),
        ds.manage_config_system_p1, md_wrk.md_new_line(),
        ds.manage_config_system_p2, md_wrk.md_new_line(),
        ds.manage_config_system_p3, md_wrk.md_new_line(),
        md_wrk.md_header_2(ds.monitoring_title),
        ds.monitoring_p1, md_wrk.md_new_line(),
        ds.monitoring_p2, md_wrk.md_new_line()
    )

    return the_page
