import documentum.md_master_page as md_master_page

def main():
    print('Hello, Document writer')

def write_readme_md():
    with open('README.md', 'w') as f:
        f.write(md_master_page.get_page() )
        f.close()

if __name__ == '__main__':
    main()
    write_readme_md()