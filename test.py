import json
import yaml

src= """
apiVersion: batch/v1
kind: CronJob
metadata:
  name: asdf
  namespace: asdf
spec:
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
            - name: aasdf
              image: aSDF
              command: ["curl"] 
              args: [
                  "-v",
                  "-H",
                  "Content-Type: application/json",
                  "-X",
                  "POST",
                  "-d",
                  '{"backup_date": "типа дата"}',
                  "https://tg.k8s.codicus.ru/message",
                  "-k",
                ]
          restartPolicy: OnFailure
"""

j = yaml.safe_load(src)

print(j)
