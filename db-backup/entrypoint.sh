#!/bin/bash

TG_BOT="https://tg.k8s.codicus.ru/message"
FILE_PATH=/mnt/backups
DUMP_FILE_EXT=sql
TAR_FILE_EXT=tar.gz

echo '$1' is: $1
echo '$2' is: $2
echo '$CI_RESTORE_DB_BCKP_FILE_NAME' is: $CI_RESTORE_DB_BCKP_FILE_NAME

function to_curl() {
    curl --silent -H "Content-Type: application/json" -X POST -d "{$DATA}" $TG_BOT -k
}

function get_backup_database() {
    FILE_NAME=$(date +%y%m%d-%H%M)-$1-$2

    mysqldump -h $DB_HOST -u $DB_USER -p$DB_PASS $2 > $FILE_PATH/$FILE_NAME.$DUMP_FILE_EXT
    echo mysql_code_is: $?
    mysql_exit_code=$?

    tar -C $FILE_PATH -cvzf $FILE_PATH/$FILE_NAME.$TAR_FILE_EXT $FILE_NAME.$DUMP_FILE_EXT --remove-files
    if [ $? -ne 0 ]
    then
        exit 1
    fi

    if [ -f $FILE_PATH/$FILE_NAME.$TAR_FILE_EXT ]
    then
        echo File $FILE_PATH/$FILE_NAME.$TAR_FILE_EXT is present
    else
        exit 1
    fi

    echo List all file in directory
    ls -l $FILE_PATH
}

function get_restore_database() {
    echo Create db
    mysql -h $DB_HOST -u $DB_USER -p$DB_PASS <<EOF
    create database if not exists gdbase;
    exit
EOF
    
    cd $FILE_PATH
    tar -xvzf $CI_RESTORE_DB_BCKP_FILE_NAME.$TAR_FILE_EXT
    if [ -f $CI_RESTORE_DB_BCKP_FILE_NAME.$TAR_FILE_EXT ]
    then
        echo File $CI_RESTORE_DB_BCKP_FILE_NAME.$TAR_FILE_EXT is present
    else
        echo File $CI_RESTORE_DB_BCKP_FILE_NAME.$TAR_FILE_EXT is not present
        exit 1
    fi

    echo Restore db
    mysql -h $DB_HOST -u $DB_USER -p$DB_PASS $2 < $CI_RESTORE_DB_BCKP_FILE_NAME.$DUMP_FILE_EXT
    mysql_exit_code=$?
    echo mysql_code_is: $?
    echo database is recovered
}

DATE=$(date)
echo Hello, $1. The date is: $DATE

case $1 in

manual-bckp)
    echo Manual backup db
    DATA="\"object_kind\": \"job\", \"job_name\": \"Ручной бэкап БД\", \"date\": \"$DATE\", "
    get_backup_database $1 $2
    ;;
manual-rstr)
    echo Manual restore db
    DATA="\"object_kind\": \"job\", \"job_name\": \"Ручное восстановление БД из бэкапа БД\", \"date\": \"$DATE\", "
    get_restore_database $1 $2
    ;;
*)
    echo Backup db by default
    DATA="\"object_kind\": \"cronjob\", \"job_name\": \"Бэкап БД\", \"date\": \"$DATE\", "
    get_backup_database $1 $2
    ;;
esac


if [ $mysql_exit_code -gt 0 ]
then
    RESULT='Ошибка. Смотри логи'
else
    RESULT="Успех"
fi

DATA=$DATA"\"result\": \"$RESULT\""

to_curl

echo Job entrypoint $1 is done!

exit 0
