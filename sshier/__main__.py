from flask import Flask
from flask import Flask, request
import os

app = Flask(__name__)


@app.route('/run', methods=['POST'])
def send_message():
    data = request.json

    if data.get('cmd') == 'start':
        print('Запускаю SSH')
        result = os.popen('sudo systemctl start ssh').read()
    elif data.get('cmd') == 'stop':
        print('Останавливаю SSH')
        result = os.popen('sudo systemctl stop ssh').read()
    else:
        result = 'Непонятно, что нужно сделать'

    print(result)

    return data
    

if __name__ == '__main__':
    print('Usage:')
    print('curl -v -H "Content-Type: application/json" -d \'{"cmd": "stop"}\' -X POST http://ip_addr:8080/run - for stop SSH')
    print('curl -v -H "Content-Type: application/json" -d \'{"cmd": "stop"}\' -X POST http://ip_addr:8080/run - for start SSH')
    app.run(host='0.0.0.0', port='8080')
