data "vkcs_compute_flavor" "gbkube" {
  name = "Basic-1-2-20"
}

data "vkcs_compute_flavor" "gbkubecluster" {
  name = "Standard-2-2"
}

data "vkcs_images_image" "gbkube" {
  name = "Ubuntu-20.04.1-202008"
}
