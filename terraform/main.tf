terraform {
    backend "local" {
        # For local usage
        # path = "/home/codicus/s3fs/terraform.tfstate"
        
        # For usage on gitlab-runner
        path = "/home/gitlab-runner/s3fs/terraform.tfstate"
    }
    required_providers {
        vkcs = {
            source = "vk-cs/vkcs"
        }
    }
}

provider "vkcs" {
    username = var.CI_CLOUD_LOGIN
    password = var.CI_CLOUD_PASSWORD
    project_id = "163206f811634c489ce7471f272f205a"
    region = "RegionOne"
}