variable "CI_CLOUD_LOGIN" {
    type = string
    description = "Login"
}

variable "CI_CLOUD_PASSWORD" {
    type = string
    description = "Password"
}
