resource "vkcs_compute_instance" "gbkubedb" {
  name                    = "gb-kube-db"
  flavor_id               = data.vkcs_compute_flavor.gbkube.id
  security_groups         = ["default", "ssh+www"]
  availability_zone       = "MS1"
  key_pair = "Ubuntu20_04"
  
  block_device {
    uuid                  = data.vkcs_images_image.gbkube.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid = vkcs_networking_network.network.id
  }

  depends_on = [
    vkcs_networking_network.network,
    vkcs_networking_subnet.network,
  ]

}
