resource "vkcs_lb_loadbalancer" "loadbalancer" {
  name = "loadbalancer"

  vip_subnet_id = "${vkcs_networking_subnet.network.id}"
  
  tags = ["tag1"]
}

resource "vkcs_lb_listener" "listener" {
  name = "listener"
  protocol = "TCP"
  protocol_port = 80
  loadbalancer_id = "${vkcs_lb_loadbalancer.loadbalancer.id}"
}

resource "vkcs_lb_listener" "listener2" {
  name = "listener"
  protocol = "TCP"
  protocol_port = 443
  loadbalancer_id = "${vkcs_lb_loadbalancer.loadbalancer.id}"
}

resource "vkcs_lb_pool" "pool" {
  name = "pool"
  protocol = "TCP"
  lb_method = "ROUND_ROBIN"
  listener_id = "${vkcs_lb_listener.listener.id}"
}

resource "vkcs_lb_pool" "pool2" {
  name = "pool2"
  protocol = "TCP"
  lb_method = "ROUND_ROBIN"
  listener_id = "${vkcs_lb_listener.listener2.id}"
}

resource "vkcs_lb_member" "member_1_80" {
  address = "${vkcs_compute_instance.gbkuben1.network[0].fixed_ip_v4}"
  protocol_port = 80
  pool_id = "${vkcs_lb_pool.pool.id}"
  subnet_id = "${vkcs_networking_subnet.network.id}"
  weight = 0
}

resource "vkcs_lb_member" "member_2_80" {
  address = "${vkcs_compute_instance.gbkuben2.network[0].fixed_ip_v4}"
  protocol_port = 80
  pool_id = "${vkcs_lb_pool.pool.id}"
  subnet_id = "${vkcs_networking_subnet.network.id}"
}

resource "vkcs_lb_member" "member_1_443" {
  address = "${vkcs_compute_instance.gbkuben1.network[0].fixed_ip_v4}"
  protocol_port = 443
  pool_id = "${vkcs_lb_pool.pool2.id}"
  subnet_id = "${vkcs_networking_subnet.network.id}"
  weight = 0
}

resource "vkcs_lb_member" "member_2_443" {
  address = "${vkcs_compute_instance.gbkuben2.network[0].fixed_ip_v4}"
  protocol_port = 443
  pool_id = "${vkcs_lb_pool.pool2.id}"
  subnet_id = "${vkcs_networking_subnet.network.id}"
}
